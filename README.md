︱[Markdown 语法总览](https://markdown.com.cn/cheat-sheet.html#%E6%80%BB%E8%A7%88)︱<br />︱[Canva](https://www.canva.cn/zh_cn/)︱[ZhiHu](https://www.zhihu.com/)︱[Bilibili](https://www.bilibili.com/)︱[Midas Civil 书橱](https://midas.yunzhan365.com/bookcase/ykmg/index.html)︱
## 学术论文写作相关网站
︱[中国知网](https://www.cnki.net/)︱[百度学术](https://xueshu.baidu.com/)︱[万方](http://new.wanfangdata.com.cn/index.html)︱<br />​

︱[中国公路学报](http://zgglxb.chd.edu.cn/CN/volumn/home.shtml)︱[建筑科学与工程学报](http://jace.chd.edu.cn/default.aspx)︱[土木工程学报](http://manu36.magtech.com.cn/Jwk_tmgcxb/CN/volumn/home.shtml)︱[土木与环境工程学报](http://qks.cqu.edu.cn/cqdxxbcn/home)︱<br />​

︱[macmillan麦克米伦](https://macmillan.com/)︱[Advances in Bridge Engineering](https://aben.springeropen.com/)︱[hindawi](https://www.hindawi.com/)︱[SAGA](https://journals.sagepub.com/) ︱<br />︱[ResearchGate](https://www.researchgate.net/)︱︱[American Society of Civil Engineers (ASCE)](https://ascelibrary.org/)︱<br />​

︱[STU Library](https://www.lib.stu.edu.cn/zy/database)︱[Google Scholar-1](https://ac.scmor.com/)︱[Google Scholar-2](http://scholar.scqylaw.com/)︱[Wikipedia-China](https://zh.wikipedia.iwiki.eu.org/wiki/Wikipedia:%E9%A6%96%E9%A1%B5)︱[Google](https://google.tiktokios.com/)<br />︱[中国学术期刊论文投稿平台](https://cb.cnki.net/journal/Search.aspx?navi=C)︱[优发表](https://www.youfabiao.com/zazhi/beidaheixin/)︱
## Translate Website
︱[Baidu Translate](https://fanyi.baidu.com/)︱[Deepl Translate](https://www.deepl.com/translator)︱
## Library Resources
︱[全国图书馆参考咨询联盟](http://www.ucdrs.net/)︱[鸠摩搜书](https://www.jiumodiary.com/)︱[重庆高校数字资源共建共享平台](http://www.cquc.net/)︱[浙江图书馆](https://www.zjlib.cn/)︱[国家自然科学基金基础知识研究库](https://ir.nsfc.gov.cn/)​︱[交通运输标准知识服务平台](http://jtysbz.yuetong.cn/ysbz/fg/index.html)︱
## Better Blog Website
︱[陆新征课题组](http://www.luxinzheng.net/gbindex.htm)︱[ANSYS结构院](http://www.ansysjgy.com/)︱[田草博客](http://www.tiancao.net/default.asp)︱[崔济东博客](http://www.jdcui.com/)︱
## Sofetware Learning
︱[ANSYS](https://www.yuque.com/putaogan-2xqp2/kb/glpwhr)︱[Origin](https://www.yuque.com/putaogan-2xqp2/kb/ob3nie)︱[MATLAB](https://www.yuque.com/putaogan-2xqp2/kb/wuvf0q)︱[Excel](https://www.yuque.com/putaogan-2xqp2/kb/ia40vi)
